<?php


function _sxip_homesite_discovery($homesite_path) {
  $path = _sxip_url_expansion($homesite_path);
  
  // check first for dix.xml
  $result = drupal_http_request($path . '/dix.xml');
  if ($result->code == '200') {

  }
  else {
    // check LINK header
    $result = drupal_http_request($path);
    if ($result->code == 200 || $result->code == 302) {
      preg_match('|<link\s+rel="dix:/homesite"(.*)/?>|iU', $result->data, $matches);
      if ($matches[1]) {
        preg_match('|href="([^"]+)"|iU', $matches[1], $href);
        preg_match('|class="([^"]+)"|iU', $matches[1], $class);
        return array('endpoint' => $href[1],
                     'capabilities' => explode(' ', $class[1]));
      }
    }
  }

  return FALSE;
}

function _sxip_url_expansion($path) {
  return 'http://' . $path;
}

function _sxip_digest($edit) {
  $pairs = array();
  $chars = array('%', '=', '&');
  $repl = array('%25', '%3D', '%26');
  foreach ($edit as $key => $val) {
    $key = str_replace($chars, $repl, $key);
    $val = str_replace($chars, $repl, $val);
    $pairs[] = $key . '='. $val;
  }
  sort($pairs);
  return sha1(implode('&', $pairs));
}

/**
 * Returns a list of the available properties.
 */
function _sxip_properties() {
  /**
   * These are all the properties that are requestable at the moment
   * ideally these should be read from a file or some other source so
   * that they are easier to update and keep in sync
   */
  $items = array('/sxip.net/namePerson/prefix' => t('Prefix'),
                 '/sxip.net/namePerson/first' => t('First Name'),
                 '/sxip.net/namePerson/last' => t('Last Name'),
                 '/sxip.net/namePerson/middle' => t('Middle Name'),
                 '/sxip.net/namePerson/suffix' => t('Suffix'),
                 '/sxip.net/namePerson/friendly' => t('Alias'),
                 '/sxip.net/birthDate/birthYear' => t('Year of Birth'),		
                 '/sxip.net/birthDate/birthMonth' => t('Month of Birth'),
                 '/sxip.net/birthDate/birthDay' => t('Day of Birth'),
                 '/sxip.net/contact/phone/default' => t('Main Phone'),
                 '/sxip.net/contact/phone/home' => t('Home Phone'),
                 '/sxip.net/contact/phone/business' => t('Business Phone'),
                 '/sxip.net/contact/phone/cell' => t('Mobile Phone'),
                 '/sxip.net/contact/phone/fax' => t('Fax'),
                 '/sxip.net/contact/IM/default' => t('Main IM'),
                 '/sxip.net/contact/IM/AIM' => t('AIM'),		
                 '/sxip.net/contact/IM/ICQ' => t('ICQ'),		
                 '/sxip.net/contact/IM/MSN' => t('MSN'),		
                 '/sxip.net/contact/IM/Yahoo' => t('Yahoo!'),		
                 '/sxip.net/contact/IM/Jabber' => t('Jabber'),		
                 '/sxip.net/contact/IM/Skype' => t('Skype'),
                 '/sxip.net/contact/internet/email' => t('Email'),
                 '/sxip.net/contact/internet/verifiedemailhash' => t('Verified Email'),
                 '/sxip.net/contact/web/default' => t('Web URL'),
                 '/sxip.net/contact/web/blog' => t('Blog URL'),
                 '/sxip.net/contact/web/Linkedin' => t('LinkedIn URL'),
                 '/sxip.net/contact/web/Amazon' => t('Amazon URL'),
                 '/sxip.net/contact/web/Flickr' => t('Flickr URL'),
                 '/sxip.net/contact/web/Delicious' => t('Delicious URL'),
                 '/sxip.net/company/name' => t('Company Name'),
                 '/sxip.net/company/title' => t('Title'),		
                 '/sxip.net/media/image/small' => t('Small Web Image URI (< 16k)'),
                 '/sxip.net/media/image/medium' => t('Medium Web Image URI (< 128k)'),
                 '/sxip.net/media/image/large' => t('Large Web Image URI (<= 1Mb)'),
                 '/sxip.net/media/spokenname' => t('Spoken Name (URI to an audio file)'),
                 '/sxip.net/media/greeting/audio' => t('Audio Greeting (URL)'),
                 '/sxip.net/media/greeting/video' => t('Video Greeting (URL)'),
                 '/sxip.net/media/biography' => t('Biography'),
      );
  
  return $items;
}
